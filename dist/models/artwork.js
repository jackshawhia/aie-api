"use strict";

var mongoose = require("mongoose");

var ArtworkSchema = new mongoose.Schema({
  art_name: String,
  author: String,
  description: String,
  art_type: String,
  // format: options(),
  // uploadDate: ontimeupdate,//上传时间 不知道这个ontimeupdate是什么
  view_times: {
    type: Number,
    "default": 0
  } // // price: String,//分免费和付费 手动设置
  // // buyer: String,//买家信息 from users/purchase_history
  // datasize: onloadedmetadata,//文件大小
  // // status: options()//available或者unavailable

}, {
  collection: "artworkdb"
});
module.exports = mongoose.model("Artwork", ArtworkSchema);