# Assignment 2 - Agile Software Practice.

Name: Zhenyu Shao
Student id: 20086425

## Client UI.

...... Show screenshots of each view/page in you Vue app - include a short caption stating the purpose of each one .......

e.g.

![][donate]

>>Allows the user to sign up

>>Allow the user to login

>>Allow the user to edit

>>All the user to view artworks

>>All the user to remove artworks

>>View forum to see others' ideas

>>View Support and get support from administrator(the owner, me)

>>Contact Us page, allow user to submit questions
## E2E/Cypress testing.

(Optional) State any non-standard features (not covered in the lectures or sample code provided) of the Cypress framework that you utilized.


## GitLab CI.

(Optional) State any non-standard features (not covered in the lectures or labs) of the GitLab CI platform that you utilized.


[donate]: ./img/donate.png