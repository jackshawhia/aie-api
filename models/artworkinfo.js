let mongoose = require("mongoose")

let ArtworkinfoSchema = new mongoose.Schema({
  comment: String,
  othertime: string,//从artwork引入
  thumbups: {
    type: Number,
    default : 0
  }
  // // price: String,//分免费和付费 手动设置
  // // buyer: String,//买家信息 from users/purchase_history
  // datasize: onloadedmetadata,//文件大小
  // // status: options()//available或者unavailable

},{collection: "artworkinfodb"})
module.exports = mongoose.model("Artworkinfo", ArtworkinfoSchema)